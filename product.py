# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date

from decimal import Decimal
from trytond.model import fields
from trytond.pool import Pool, PoolMeta


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    cost_w_tax = fields.Function(fields.Numeric('Cost With Tax', digits=(16, 2),
        depends=['list_price', 'customer_taxes']), 'on_change_with_cost_w_tax')

    def on_change_with_cost_w_tax(self, name=None):
        if self.cost_price:
            res = self.compute_cost_w_tax(self.cost_price)
            return res

    def compute_cost_w_tax(self, cost_price):
        Tax = Pool().get('account.tax')
        res = cost_price
        positive_taxes = [t for t in self.supplier_taxes_used if (t.type == 'percentage' and t.rate > 0)]
        if positive_taxes and cost_price:
            date_ = date.today()
            tax_list = Tax.compute(positive_taxes,
                cost_price or Decimal('0.0'), 1, date_)
            res = sum([t['amount'] for t in tax_list], Decimal('0.0'))
            res = res + cost_price
        res = res.quantize(
            Decimal(1) / 10 ** self.__class__.cost_w_tax.digits[1])
        return res
