# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import purchase
from . import move
from . import product

def register():
    Pool.register(
        purchase.PurchaseLine,
        purchase.Configuration,
        move.Move,
        purchase.Purchase,
        purchase.PurchaseDiscountWizardStart,
        product.Template,
        module='purchase_discount', type_='model')
    Pool.register(
        purchase.PurchaseDiscountWizard,
        module='purchase_discount', type_='wizard')
    Pool.register(
        purchase.PurchaseDiscountReport,
        module='purchase_discount', type_='report')
